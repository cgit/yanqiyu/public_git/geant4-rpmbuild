A unoffical Geant4 build for Fedora. 

[![Copr build status](https://copr.fedorainfracloud.org/coprs/yanqiyu/geant4/package/geant4/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/yanqiyu/geant4/package/geant4/)

``` Bash
sudo dnf enable yanqiyu/geant4
sudo dnf install geant4 geant4-devel geant4-data
```